import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm';
import { UserService } from './user-service';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Injectable()
export class fcmTokenService {
  constructor(private userService: UserService, private fcm: FCM, private localNotifications: LocalNotifications) {
  }

  tokenInitializer() {
    this.fcm.subscribeToTopic('marketing');

    this.fcm.getToken().then(token => {
      this.userService.registerDeviceToken(token);
    })

    this.fcm.onNotification().subscribe((data:any) => {
      if (data.wasTapped) {
        console.log("Received in background");
      } else {
        this.localNotifications.schedule({
          text: "You have a new message.",
          at: new Date(new Date().getTime() + 3600),
          led: 'FF0000',
        });
      };
    })

    this.fcm.onTokenRefresh().subscribe(token => {
      this.userService.registerDeviceToken(token);
    })

    this.fcm.unsubscribeFromTopic('marketing');
  }
}
