import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs';
import { AngularFire } from "angularfire2";

@Injectable()
export class UserService {
  userDetails: any;

  constructor(public http: Http, public afRef: AngularFire) {
  }

  private userRole: any = {};
  private isAuthenticated = false;
  private userObj: any = {};
  private deviceToken: string = "";
  public deviceTokenObserver$: Subject<any> = new Subject<any>();
  public userObserver$: Subject<any> = new Subject<any>();

  getUserDetails() {
    this.isAuthenticated = true;
    if (this.userObj.$key)
      return this.userObj;
    else if (localStorage.getItem("userObj")) {
      this.userObj = JSON.parse(localStorage.getItem("userObj"));
      this.userObj.$key = this.userObj.myKey;
      return this.userObj;
    }
  }

  setUserDetails(userObj) {
    this.userObj = userObj;
    this.isAuthenticated = true;
    localStorage.setItem("userObj", JSON.stringify(userObj));
    this.userObserver$.next(userObj);
  }

  getUserRole() {
    return this.userRole;
  }

  setUserRole(userRole) {
    this.userRole = userRole;
  }

  isAlreadyAuthenticated() {
    return this.isAuthenticated ? true : false;
  }

  registerDeviceToken(token) {
    if ((token && token.trim() == "") || !token)
      return false;
    else {
      this.deviceToken = token || "N/A";
      console.log("-------------Token-----------------");
      console.log(token);
      console.log("-------------Token-----------------");
      this.deviceTokenObserver$.next(this.deviceToken);
      if (token && token != "N/A" && token.trim() != "") {
        this.userDetails = this.getUserDetails();
        if (this.userDetails.$key || this.userDetails.myKey) {
          this.afRef.database.list("/users").update(this.userDetails.$key || this.userDetails.myKey, { deviceToken: token }).then(() => {
            console.log("succeccfully updated")
          }).catch((err) => {
            console.log(err.message);
          })
        }
      }
    }
  }

  logout() {
    this.isAuthenticated = false;
    this.userObj = {};
    localStorage.removeItem("userObj");
  }
}
