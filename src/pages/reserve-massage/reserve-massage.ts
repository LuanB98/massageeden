import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { MessageDetailPage } from "../message-detail/message-detail";

@Component({
  selector: 'page-reserve-massage',
  templateUrl: 'reserve-massage.html'
})
export class ReserveMassagePage {

  event = {
    timeStarts: '20:00'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReserveMassagePage');
  }

  messageProvider(){
    this.navCtrl.push(MessageDetailPage)
  }

  confirmBooking(){
    let confirm = this.alertCtrl.create({
      title: 'Confirm Booking',
      message: 'Are you sure? You want to confirm this booking?',
      buttons: [
        {
          text: 'Confirm',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Cancel',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
}
