import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyAppointmentDetailsPage } from "../my-appointment-details/my-appointment-details";
import { UserService } from "../../providers/user-service";
import { AngularFire } from "angularfire2";
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-my-appointments',
  templateUrl: 'my-appointments.html'
})
export class MyAppointmentsPage {

  myAppointments = [];

  userObj: any = {};
  providerRef: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService,
     public afRef: AngularFire, private geolocation: Geolocation) {
    this.userObj = this.userService.getUserDetails();
    this.getLocation(this.userObj.$key || this.userObj.userKey);
    let appointmentNodeRef = this.afRef.database;
    var options = { weekday: 'short', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' };

    this.myAppointments = [];
    this.providerRef = appointmentNodeRef.list("/users/" + this.userObj.$key + "/myAppointments")
      .mergeMap(appointmentRef => appointmentRef.map(obj =>
        appointmentNodeRef.object("/appointments/" + obj.$value)
      ))
      .mergeMap((appointments: any) => appointments.map(d => d))
      .subscribe((appointmentObj: any) => {
        console.log(appointmentObj);
        appointmentObj.dateTime = new Date(appointmentObj.dateTime).toLocaleString("en-US", options);
        this.myAppointments.push(appointmentObj);
      })

    if (this.userObj.role == "provider") {
      this.userObj.enableAppointmentMenu = true;
      this.userService.userObserver$.subscribe(data => {
        console.log(data);
      })
    }
    else this.userObj.enableAppointmentMenu = true;

  }

  getTitleAppointment(currentAppObj){
    return this.userObj.role=="client"?  currentAppObj.providerName : currentAppObj.clientName;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAppointmentsPage');
    this.userService.userObserver$.subscribe(user=>{
      this.userObj = user;
    });
  }

  getLocation(userKey) {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log({latitude:resp.coords.latitude, longitude: resp.coords.longitude});
      this.afRef.database.list("/users").update(userKey, { userLocation: {latitude:resp.coords.latitude, longitude: resp.coords.longitude} })
        
    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  gotoAppointmentDetails(appointmentObj) {
    this.navCtrl.push(MyAppointmentDetailsPage, { appointment: appointmentObj });
  }

}
