import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
declare var paypal: any;
import { PaymentService } from '../../providers/payment-service';
import { AngularFire } from 'angularfire2';
import { UserService } from "../../providers/user-service";
// import * as braintree from 'braintree-web-drop-in';
declare var braintree: any;
@Component({
  selector: 'page-user-payment',
  templateUrl: 'user-payment.html'
})
export class UserPaymentPage {

  amount = "";
  userObj: any = {};
  appointmentRef: any = {};
  appointmentObj: any = {};
  showFormFields = false;
  braintreeInstance: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public paymentService: PaymentService, public afRef: AngularFire) {
    this.userObj = this.userService.getUserDetails();
    this.amount = this.navParams.get("amount");
    this.appointmentObj = this.navParams.get("appointmentObj");

    let that = this;
    paymentService.getToken().subscribe((clientToken: any) => {
      that = this;
      braintree.client.create({
        authorization: clientToken._body
      }, (clientErr, clientInstance) => {
        if (clientErr) {
          console.error('Error creating client:', clientErr);
          return
        }

        braintree.paypalCheckout.create({
          client: clientInstance
        }, (paypalCheckoutErr, paypalCheckoutInstance) => {
          if (paypalCheckoutErr) {
            console.error('Error creating PayPal Checkout:', paypalCheckoutErr);
            return;
          }

          paypal.Button.render({
            env: 'sandbox', // or 'sandbox'
            payment: () => {
              return paypalCheckoutInstance.createPayment({
                flow: 'checkout', // Required
                amount: that.amount, // Required
                currency: 'USD' // Required
              });
            },
            onAuthorize: (data, actions) => {

              return paypalCheckoutInstance.tokenizePayment(data)
                .then((payload) => {
                  // Submit `payload.nonce` to your server
                  let paymentObj = {
                    nonce: payload.nonce,
                    amount: this.amount,
                    provider_id: this.appointmentObj.providerId,
                    provider_amount: this.amount,
                    provider_email: "temp@email.com",
                    provider_name: this.appointmentObj.providerName,
                    transaction_date: new Date().toISOString()
                  }
                  this.paymentService.sendToken(paymentObj).subscribe((result: any) => {
                    console.log(result);
                    console.log(this.appointmentObj)
                    this.appointmentRef = this.afRef.database.list("/appointments/");
                    this.appointmentRef.update(this.appointmentObj.$key, { isBooked: true });
                    this.navCtrl.pop();
                  })
                });
            },
          }, '#paypal-button');
        })
      })
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPaymentPage');
  }

}
