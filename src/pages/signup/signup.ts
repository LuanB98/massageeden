import { Component } from '@angular/core';
import {AngularFire} from 'angularfire2'
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from "../login/login";

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  /*profileStatus consist of three parts
    "new" = Needs account settings and not publish
    "published" = Available to search
    "suspended" = Inactive due to any activity
  */

  newUserObj:any = {
    fullName: '',
    email: '',
    password: '',
    confirmPassword: '',
    role: 'client'
  };
  constructor(public navCtrl: NavController, public navParams: NavParams,public afRef:AngularFire) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  enableSignup(){
    return this.newUserObj.email.trim() != '' && this.newUserObj.fullName.trim() != '' &&
      (this.newUserObj.password.trim() != '' && this.newUserObj.confirmPassword.trim() != '' ) &&
      (this.newUserObj.password.trim() == this.newUserObj.confirmPassword.trim());
  }

  createUser(){
    this.newUserObj.email = this.newUserObj.email.toLowerCase();
    this.afRef.auth.createUser({email: this.newUserObj.email, password: this.newUserObj.password}).then(data=> {
      delete this.newUserObj.confirmPassword;

      if(this.newUserObj.role == "provider"){
        this.newUserObj.profileStatus= 'new';
      }

      this.afRef.database.object("/users/" + data.uid).set(this.newUserObj);
      alert("Successfully user created ");

      //clear data fields
      this.newUserObj = {
        fullName: '',
        email: '',
        password: '',
        confirmPassword: '',
        role: 'client',
        profileStatus: ''
      };
      this.navCtrl.push(LoginPage);
    });
  }
}
