import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {MessageDetailPage} from "../message-detail/message-detail";
import {AngularFire} from "angularfire2";
import {UserService} from "../../providers/user-service";
import * as moment from 'moment';

@Component({
  selector: 'page-messages-list',
  templateUrl: 'messages-list.html'
})
export class MessagesListPage {

  messageChannelRef: any = [];
  userMessages: any = [];
  userObj: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public afRef: AngularFire,) {
    this.userObj = this.userService.getUserDetails();
  }

  ionViewDidLoad() {
    this.afRef.database.list("/messageChannels")
      .subscribe(data=> {
        this.userMessages = data.filter(msgObj=>{
          if(msgObj.senderId == this.userObj.$key || msgObj.receiverId == this.userObj.$key) {
            msgObj.messageTitle =  msgObj.senderId == this.userObj.$key? msgObj.receiverName : msgObj.senderName;
            return msgObj;
          }
        });

        if(this.userMessages.find(obj=> obj.messages)){
          this.userMessages = this.userMessages.map(msgObj=>{
              msgObj.messages = Object.keys(msgObj.messages).map(function (key) { return msgObj.messages[key]; });
              msgObj.lastMsg = msgObj.messages.slice(msgObj.messages.length-1)[0].text
              return msgObj;
          });
      }
      console.log(this.userMessages);
    });
  }

  getTimeAgo(timeStamp){
    return moment(timeStamp).fromNow();
  }
  gotoChat(msgObj) {
    let param:any = {};
    param.$key = msgObj.senderId == this.userObj.$key? msgObj.receiverId : msgObj.senderId;
    param.fullName = msgObj.messageTitle;
    if(msgObj.receiverId == this.userObj.$key && !msgObj.seenByReceiver){
      msgObj.seenByReceiver = true;
      let msgRef = this.afRef.database.list("/messageChannels")
      msgRef.update(msgObj.$key,{ seenByReceiver: true })
    }
    this.navCtrl.push(MessageDetailPage, 
      {"providerInfo": param, "channelNode": msgObj.$key, "messageTitle": msgObj.messageTitle, "msgObj": msgObj}
    )
  }

}
