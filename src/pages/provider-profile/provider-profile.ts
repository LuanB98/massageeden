import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ReserveMassagePage } from "../reserve-massage/reserve-massage";
import { MessageDetailPage } from "../message-detail/message-detail";

/*
  Generated class for the ProviderProfile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-provider-profile',
  templateUrl: 'provider-profile.html'
})
export class ProviderProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProviderProfilePage');
  }

  bookReservation(){
    this.navCtrl.push(ReserveMassagePage)
  }

  messageProvider(){
    this.navCtrl.push(MessageDetailPage)
  }
}
