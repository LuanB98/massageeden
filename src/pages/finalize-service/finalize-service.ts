import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-finalize-service',
  templateUrl: 'finalize-service.html'
})
export class FinalizeServicePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinalizeServicePage');
  }

}
