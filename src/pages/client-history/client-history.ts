import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MessageDetailPage } from "../message-detail/message-detail";
import {UserService} from "../../providers/user-service";


@Component({
  selector: 'page-client-history',
  templateUrl: 'client-history.html'
})
export class ClientHistoryPage {

  senderObj:any = {};
  channelNode:string = "";
  messageTitle = "";
  msgObj:any
  userObj:any ={}
  route = "";
  clientName;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService) {
    this.senderObj = this.navParams.get("providerInfo");
    this.channelNode = this.navParams.get("channelNode");
    this.messageTitle = this.navParams.get("messageTitle");
    this.msgObj = this.navParams.get("msgObj");
    this.route = this.navParams.get("route");
    if(this.route === 'appointmentDetails'){
      this.msgObj = this.navParams.get("msgObj"); 
      this.clientName = this.navParams.get("clientName")           
    }
    else{
      this.clientName = this.navParams.get("messageTitle");
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientHistoryPage');
     this.userObj = this.userService.getUserDetails();
  }

  messageProvider(){

    let param:any = {};
    param.$key = this.msgObj.senderId == this.userObj.$key? this.msgObj.receiverId : this.msgObj.senderId;
    param.fullName = this.msgObj.messageTitle;
    // console.log('message', this.msgObj.messageTitle)
    // this.navCtrl.push(MessageDetailPage, {"providerInfo": param, "channelNode": msgObj.$key, "messageTitle": msgObj.messageTitle})
    if(this.route === 'appointmentDetails'){
        this.navCtrl.push(MessageDetailPage,{providerInfo: {$key : this.msgObj.$key, name: this.msgObj.name, clientName: this.msgObj.clientName,  route:"appointmentDetails" }})
    }
    else{
        this.navCtrl.push(MessageDetailPage,{"providerInfo": param, "channelNode": this.msgObj.$key, "messageTitle": this.msgObj.messageTitle}) 
    }
  }
}
