import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ProviderProfilePage } from "../provider-profile/provider-profile";
import { ReserveMassagePage } from "../reserve-massage/reserve-massage";
import { MessageDetailPage } from "../message-detail/message-detail";
import { AngularFire } from "angularfire2";
import { UserService } from "../../providers/user-service";
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-dashbord',
  templateUrl: 'dashbord.html'
})
export class DashbordPage {

  searchQuery: string = '';
  items: any;
  providerList = [];
  userDetails: any = {};
  isSearchEnd: boolean;
  providerDefaultRadius = 20;  // Default service location radius for provider (in km)

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController,
    public afRef: AngularFire, public loadingCtrl: LoadingController, public userService: UserService, private geolocation: Geolocation) {

  }

  ionViewDidLoad() {
    this.menuCtrl.enable(true);
    this.userDetails = this.userService.getUserDetails();
    if (this.userDetails.$key) {
      this.getLocation(this.userDetails.$key);
    }
    this.getAllProviders(this.userDetails.userLocation);
  }

  getLocation(userKey) {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log({ latitude: resp.coords.latitude, longitude: resp.coords.longitude });
      this.afRef.database.list("/users").update(userKey, { userLocation: { latitude: resp.coords.latitude, longitude: resp.coords.longitude } })

    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  initializeItems() {
    /*this.items = [
        {name: 'Amsterdam', gender: 'Female', location: 'North Perth', avgRates: '40', desc: "I'm giving world's best services to my clients...", experience: 'More than 5 years',
          serviceDetail : {availability: 'Office/Home', thaiMassage: '1 hr for $40', sportsMassage: '1 hr for $55'}},
        {name: 'Britney',gender: 'Female', location: 'East Perth', avgRates: '30', desc: "I'm giving world's best services to my clients...", experience: 'More than 5 years',
          serviceDetail : {availability: 'Office/Home', thaiMassage: '1 hr for $40', sportsMassage: '1 hr for $55'}},
        {name: 'Camilla',gender: 'Female', location: 'North Perth', avgRates: '30', desc: "I'm giving world's best services to my clients...", experience: 'More than 5 years',
          serviceDetail : {availability: 'Office/Home', thaiMassage: '1 hr for $40', sportsMassage: '1 hr for $55'}},
        {name: 'Janet',gender: 'Female', location: 'North Perth', avgRates: '55', desc: "I'm giving world's best services to my clients...", experience: 'More than 5 years',
          serviceDetail : {availability: 'Office/Home', thaiMassage: '1 hr for $40', sportsMassage: '1 hr for $55'}},
        {name: 'Katie',gender: 'Female', location: 'North Perth', avgRates: '45', desc: "I'm giving world's best services to my clients...", experience: 'More than 5 years',
          serviceDetail : {availability: 'Office/Home', thaiMassage: '1 hr for $40', sportsMassage: '1 hr for $55'}},
        {name: 'Leona',gender: 'Female', location: 'East Perth', avgRates: '90', desc: "I'm giving world's best services to my clients...", experience: 'More than 5 years',
          serviceDetail : {availability: 'Office/Home', thaiMassage: '1 hr for $40', sportsMassage: '1 hr for $55'}},
       ];*/
  }

  getItems(ev: any) {
    this.initializeItems();
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  gotoProvoderProfile(item) {
    console.log(item);
    this.navCtrl.push(ProviderProfilePage);
  }

  bookReservation() {
    this.navCtrl.push(ReserveMassagePage)
  }

  messageProvider(providerObj) {
    this.navCtrl.push(MessageDetailPage, { "providerInfo": providerObj })
  }

  getAllProviders(userCoordinates: any) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      showBackdrop: false
    });
    this.isSearchEnd = false;
    loading.present();
    this.afRef.database.list('/users', {
      query: {
        orderByChild: 'role',
        equalTo: 'provider',
      }
    }).subscribe(data => {
      loading.dismiss();
      this.providerList = data.filter(provider => {
        if (provider.profileStatus && provider.profileStatus == "published") {
          if (provider.userLocation && this.userDetails.userLocation)
            var userDistance = this.getDistance(
              this.userDetails.userLocation.latitude, this.userDetails.userLocation.longitude,
              provider.userLocation.latitude, provider.userLocation.longitude);
          console.log(`user is ${userDistance} away from provider`);
          if ((userDistance || userDistance == 0) && (parseInt(provider.serviceRadius) || this.providerDefaultRadius) >= userDistance)
            return provider;
        }
      });
      this.isSearchEnd = true;
    });
  }

  getDistance(lat1, lon1, lat2, lon2) {
    let R = 6371; // Radius of the earth in km
    let dLat = deg2rad(lat2 - lat1);  // deg2rad below
    let dLon = deg2rad(lon2 - lon1);
    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km

    function deg2rad(deg) {
      return deg * (Math.PI / 180)
    }

    return d;
  }



}
