import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MessageDetailPage } from "../message-detail/message-detail";
import { UserService } from "../../providers/user-service";
import { AngularFire } from "angularfire2";

@Component({
  selector: 'page-service-quote',
  templateUrl: 'service-quote.html'
})
export class ServiceQuotePage {

  event: any = {
    serviceCharges: "10",
    serviceDetail: '1hr Thai Massage',
    location: '123 Main street, North Perth WA 6000',
    providerName: "",
    clientName: "",
    desc: "N/A",
    clientId: '',
    providerId: '',
    dateTime: new Date().toISOString(),
    isBooked: false,
    serviceTaken: false,
    seenByClient: false,
    seenByProvider: false,
  };
  messageTitle: ""
  userObj: any = {};
  appointmentNodeRef: any = {};
  providerRef: any = {};
  clientRef: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public afRef: AngularFire) {
    this.userObj = userService.getUserDetails();
    this.event.providerName = this.userObj.fullName;
    this.event.clientId = this.navParams.get("clientId") || 'N/A';
    if (this.navParams.get("route") === 'appointmentDetails') {
      this.event.clientName = this.navParams.get("clientName")
    }
    else {
      this.event.clientName = this.navParams.get("messageTitle");
    }

    this.event.providerId = this.userObj.$key || 'N/A';

    this.appointmentNodeRef = this.afRef.database.list("/appointments");
    this.providerRef = this.afRef.database.list("/users/" + this.userObj.$key + "/myAppointments");
    this.clientRef = this.afRef.database.list("/users/" + this.event.clientId + "/myAppointments");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceQuotePage');
  }

  finalizeBooking() {
    if (this.event.dateTime.trim() != "" && this.event.serviceCharges.trim() != "" && this.event.serviceDetail.trim() != "" &&
      this.event.location.trim() != "" && this.event.providerName.trim() != "" && this.event.desc.trim() != "") {

      this.appointmentNodeRef.push(this.event).then(d => {
        this.providerRef.push(d.getKey());
        this.clientRef.push(d.getKey());
        alert("Appointment has been successfully created.");
      })

    }
    else alert("Please fill all details");
    this.navCtrl.pop();

  }

  messageProvider() {
    this.navCtrl.push(MessageDetailPage)
  }
}
