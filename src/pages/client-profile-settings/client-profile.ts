import { Component, Inject } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserService } from "../../providers/user-service";
import { AngularFire, FirebaseApp } from "angularfire2";
import { Camera } from '@ionic-native/camera';

@Component({
  selector: 'page-client-profile',
  templateUrl: 'client-profile.html'
})
export class ClientProfilePage {
  userObj: any = {
    gender: 'male',
    rating: 5
  };

  private usersRef: any = {};
  public storageRef: any;
  fileName: any;
  showEditMode = true;
  constructor( @Inject(FirebaseApp) firebaseApp, private camera: Camera, public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public afRef: AngularFire) {
    this.userObj = userService.getUserDetails();
    this.userObj.rating = 5;
    this.storageRef = firebaseApp.storage().ref();
    console.log(this.userObj)
    if (!this.userObj.fullName) {
      this.userObj.fullName = "N/A";
    }
    if (!this.userObj.gender) {
      this.userObj.gender = "Male";
    }
    if (!this.userObj.rating) {
      this.userObj.rating = 5;
    }

    this.usersRef = this.afRef.database.list("/users");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientProfilePage');
  }
  selectPicture() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality: 100,
      encodingType: this.camera.EncodingType.JPEG,
    }).then(imageData => {
      this.storageRef.child(this.userObj.$key).putString(imageData, 'base64', { contentType: 'image/jpeg' }).
        then(data => {
          // alert(data.downloadURL);
          this.userObj.imageUrl = data.downloadURL;
          this.savePicture();
        })
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }
  saveChanges() {
    this.showEditMode = true;
    let key: String = this.userObj.$key;
    this.usersRef.update(key, { gender: this.userObj.gender, rating: this.userObj.rating, fullName: this.userObj.fullName, imageUrl: this.userObj.imageUrl })
      .then(data => {
        alert("Saved changes successfully");
      }, err => {
        alert(err.message);
      })
  }
  savePicture() {
    this.showEditMode = true;
    let key: String = this.userObj.$key;
    this.usersRef.update(key, {imageUrl: this.userObj.imageUrl })
      .then(data => {
        alert("Saved Profile Picture successfully");
      }, err => {
        alert(err.message);
      })
  }
}
