import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from "../pages/menu/menu";
import { fcmTokenService } from '../providers/fcm-token-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  enableNotification: boolean;

  constructor(platform: Platform, public fcmToken:fcmTokenService  ) {
    if (localStorage.getItem("userObj")) {
      this.rootPage = MenuPage;
    }
    else this.rootPage = LoginPage;

    if (platform.is('mobileweb') || platform.is('core')) {
      this.enableNotification = false;
    }
    else this.enableNotification = true;

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();

      if (this.enableNotification) {
        // this.fcmToken.tokenInitializer()
      }
    });
  }


}
