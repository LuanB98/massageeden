import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { DashbordPage } from "../pages/dashbord/dashbord";
import { MenuPage } from "../pages/menu/menu";
import { ProviderProfilePage } from "../pages/provider-profile/provider-profile";
import { ReserveMassagePage } from "../pages/reserve-massage/reserve-massage";
import { MyAppointmentsPage } from "../pages/my-appointments/my-appointments";
import { ClientProfilePage } from "../pages/client-profile-settings/client-profile";
import { MessagesListPage } from "../pages/messages-list/messages-list";
import { MessageDetailPage } from "../pages/message-detail/message-detail";
import { MyAppointmentDetailsPage } from "../pages/my-appointment-details/my-appointment-details";
import { UserService } from "../providers/user-service";
import { fcmTokenService } from '../providers/fcm-token-service';
import { ProviderProfileSettingsPage } from "../pages/provider-profile-settings/provider-profile-settings";
import { ServiceQuotePage } from "../pages/service-quote/service-quote";
import { FinalizeServicePage } from "../pages/finalize-service/finalize-service";
import { ClientHistoryPage } from "../pages/client-history/client-history";
import { SignupPage } from "../pages/signup/signup";
import { UserPaymentPage } from "../pages/user-payment/user-payment";
import { AngularFireModule } from 'angularfire2';
import { firebaseConfig, myFirebaseAuthConfig } from './../config/firebase.config';
import { PaymentService } from '../providers/payment-service';
import { FCM } from '@ionic-native/fcm';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera} from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { LocalNotifications } from '@ionic-native/local-notifications'
@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DashbordPage,
    MenuPage,
    ProviderProfilePage,
    ReserveMassagePage,
    MyAppointmentsPage,
    ClientProfilePage,
    MessagesListPage,
    MessageDetailPage,
    MyAppointmentDetailsPage,
    ProviderProfileSettingsPage,
    ServiceQuotePage,
    FinalizeServicePage,
    ClientHistoryPage,
    SignupPage,
    UserPaymentPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig,myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DashbordPage,
    MenuPage,
    ProviderProfilePage,
    ReserveMassagePage,
    MyAppointmentsPage,
    ClientProfilePage,
    MessagesListPage,
    MessageDetailPage,
    MyAppointmentDetailsPage,
    ProviderProfileSettingsPage,
    ServiceQuotePage,
    FinalizeServicePage,
    ClientHistoryPage,
    SignupPage,
    UserPaymentPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},Camera,Geolocation,UserService,fcmTokenService,ImagePicker, PaymentService, LocalNotifications, FCM]
})
export class AppModule {}
