var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var braintree = require("braintree");
const cors = require('cors');

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "4gr3sw5hfbdydn2n",
  publicKey: "dtd7fwqg5wd7w3tj",
  privateKey: "1cf47c9f5ffd90d68179fb88551d23f0"
})

app.get("/",  (req, res)=> {
    res.send('message api is running')
});

app.get("/client_token",  (req, res)=> {
  gateway.clientToken.generate({},  (err, response)=> {
    res.send(response.clientToken);
  });
});

app.post("/checkout",  (req, res)=> {
    console.log('obj',req.body)
    gateway.transaction.sale({
          amount: req.body.amount,
          paymentMethodNonce: req.body.nonce,
          customFields : {
                provider_id:req.body. provider_id,
                provider_amount: req.body.amount,
                provider_email: "temp@email.com",
                provider_name: req.body.provider_name,
                transaction_date: new Date().toISOString()
              }
        }, function (err, result) {
            res.send(result)
        });
});

app.listen( process.env.PORT || '3000',()=>{
    console.log('server is running')
})
