const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.newAppointmentAlert = functions.database.ref('/appointments/{appointmentUid}').onWrite(event => {
  const userUid = event.params.userUid;

  //newly added
  //client
  //provider

  // If un-follow we exit the function.
  if (!event.data.val()) {
    return console.log('User not present with userUid: ', userUid);
  }
  console.log('Node exist with ', event.data.val());
  let appointment = event.data.val();

  // Get the list of device notification tokens.
  const getClientDeviceId = admin.database().ref(`/users/${appointment.clientId}/deviceToken`).once('value');
  const getProviderDeviceId = admin.database().ref(`/users/${appointment.providerId}/deviceToken`).once('value');

  return Promise.all([getClientDeviceId, getProviderDeviceId]).then(results => {
    const token1 = results[0].val();
    const token2 = results[1].val();

    console.log("Got results from results 1:", token1);
    console.log("Got results from results 2:", token2);

    let tokens = []
    token1 ? tokens.push(token1) : "";
    token2 ? tokens.push(token2) : "";

    if (!tokens.length) {
      console.log("you do not have any token");
      return false;
    }
    // Notification details.
    const payload = {
      notification: {
        title: 'Eden Massage',
        body: `You have a appointment from Eden Massage.`
      }
    };

    console.log("alll notifications tokens", tokens)
    // Send notifications to all tokens.
    return admin.messaging().sendToDevice(tokens, payload).then(response => {
      // For each message check if there was an error.
      const tokensToRemove = [];
      response.results.forEach((result, index) => {
        const error = result.error;
        if (error) {
          console.error('Failure sending notification to', tokens[index], error);
          // Cleanup the tokens who are not registered anymore.
          if (error.code === 'messaging/invalid-registration-token' ||
            error.code === 'messaging/registration-token-not-registered') {
            tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
          }
        }
      });
      return Promise.all(tokensToRemove);
    });
  });
});

exports.newMessageAlert = functions.database.ref('/messageChannels/{messageChannelUid}/receiverId').onWrite(event => {
  const userUid = event.params.userUid;

  //newly added
  //client
  //provider

  // If un-follow we exit the function.
  if (!event.data.val()) {
    return console.log('User not present with userUid: ', userUid);
  }
  
  let receiverId = event.data.val();
  console.log('Node exist! going to send notification on: ', receiverId);

  // Get the list of device notification tokens.
  const getReceiverDeviceId = admin.database().ref(`/users/${receiverId}/deviceToken`).once('value');

  return Promise.all([getReceiverDeviceId]).then(results => {
    const token1 = results[0].val();

    console.log("Got results from getReceiverDeviceId:", token1);

    let tokens = []
    token1 ? tokens.push(token1) : "";
    if(token1){
      tokens = [token1]
    }

    if (!tokens.length) {
      console.log("you do not have any token");
      return false;
    }
    // Notification details.
    const payload = {
      notification: {
        title: 'Eden Massage',
        body: `You have a new message.`,
        sound: "default", 
        click_action: "FCM_PLUGIN_ACTIVITY",
        priority: "high"
      }
    };

    console.log("all notifications tokens", tokens)
    // Send notifications to all tokens.
    return admin.messaging().sendToDevice(tokens, payload).then(response => {
      // For each message check if there was an error.
      const tokensToRemove = [];
      response.results.forEach((result, index) => {
        const error = result.error;
        if (error) {
          console.error('Failure sending notification to', tokens[index], error);
          // Cleanup the tokens who are not registered anymore.
          if (error.code === 'messaging/invalid-registration-token' ||
            error.code === 'messaging/registration-token-not-registered') {
            tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
          }
        }
      });
      return Promise.all(tokensToRemove);
    });
  });
});